[33mcommit e8b3ae600f15c5f308bad32e668258ed929a8f7c[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Merge: 9e3ff40 7a95a3c
Author: dabaldassi <dabaldassi@etu.uca.fr>
Date:   Sat Mar 3 18:20:45 2018 +0100

    Merge branch 'master' of gitlab.com:JAMPixel/gauffres

[33mcommit 9e3ff40e882481cdd370479182ecb10c92ff876b[m
Author: dabaldassi <dabaldassi@etu.uca.fr>
Date:   Sat Mar 3 18:20:29 2018 +0100

    ADD : actor (objet, perso)

[33mcommit 7a95a3c3635e737e9b54b0494d384a9f84ae9208[m
Author: Papin <alexpapin68@gmail.com>
Date:   Sat Mar 3 14:00:17 2018 +0100

    Fix fullscreen when resuming app

[33mcommit 9d869eeab9a3d369b0ba1bf9d4d08b887c819f31[m
Author: Papin <alexpapin68@gmail.com>
Date:   Sat Mar 3 13:16:29 2018 +0100

    Add fullscreeen

[33mcommit f4d53cbe71e303eb8d834b3108c148682aaa554b[m
Author: Papin <alexpapin68@gmail.com>
Date:   Sat Mar 3 12:05:53 2018 +0100

    Init Android project

[33mcommit 11ebb588fa07db3bfd413e1ede0c9560b07f498a[m
Author: Papin <alexpapin68@gmail.com>
Date:   Sat Mar 3 11:47:42 2018 +0100

    Add .gitignore

[33mcommit cb1a10d6e1f82114e7dac6c430d7677e97ac8ed5[m
Author: Papin <alexpapin68@gmail.com>
Date:   Sat Mar 3 11:39:25 2018 +0100

    Initial commit
