gauffres

################################################################################

Le Jetpack de ses morts

Menu principal : appuyer sur PLAY pour jouer
                 appuyer sur SCORES pour voir votre meilleur score

Jeu:

Petite animation de décollage au début.
Commandes : pencher le téléphone pour diriger le personnage.

Le but est d'avoir le meilleur score. Votre score est visible en haut de
l'écran. Votre score augmente lorsque le personnage touche des étoiles
(50 points) ou  de la bière (100 points). Lorsque le personnage touche une
bière, il augmente son taux d'alcoolémie (visible en bas à droite) et vous fait
perdre le contrôle du personnage.
En haut à droite se trouve la vie du personnage. Il en perd s'il touche des
nuages.

Le bouton back sur l'écran du jeu ou des scores vous ramène au menu principal.

Have fun !