package jampixel.me.gauffres;

/**
 * Created by david on 03/03/18.
 */

public class Beer extends Objet {
    public Beer(int id, float x, float y, int w, int h) {
        super(id, x, y, w, h);
    }

    public void effect(Perso perso) {
        perso.points += 100;
        perso.alcohol += 10;
    }
}
