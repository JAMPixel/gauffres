package jampixel.me.gauffres;

/**
 * Created by david on 03/03/18.
 */

public abstract class Actor {
    protected float speed = 10f;
    public final int id;
    public boolean toDelete = false;
    public float x;
    public float y;
    public int w;
    public int h;
    /* TODO : add stage */

    public Actor(int id, int w, int h) {
        this.w = w;
        this.h = h;
        this.id = id;
    }

    abstract void update();
}
