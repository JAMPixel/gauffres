package jampixel.me.gauffres;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by alex on 03/03/18.
 */

public class GameFragment extends Fragment implements SensorEventListener{

    private ImageView mImageChar;
    private Stage mStage = new Stage();
    private Perso mPerso = null;
    private SensorThread mThread;
    private SensorManager _sensors;
    private Sensor _accelero;
    private View rootView;
    private int actorId = 0;
    private float acc = 0;
    private int waitingTime = 0;
    public TextView mScore;
    public ProgressBar mAlcohol, mLife;
    public ArrayList<ImageView> sprites = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        _sensors = (SensorManager) getActivity().getBaseContext().getSystemService(Context.SENSOR_SERVICE);
        _accelero = _sensors.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        _sensors.registerListener(this, _accelero, SensorManager.SENSOR_DELAY_NORMAL);

        rootView = inflater.inflate(R.layout.fragment_game, container, false);
        mImageChar = rootView.findViewById(R.id.image_char);
        ((AnimationDrawable) mImageChar.getBackground()).start();
        mScore = rootView.findViewById(R.id.text_score);
        mAlcohol = rootView.findViewById(R.id.bar_alcohol);
        mLife = rootView.findViewById(R.id.bar_life);

        getFragmentManager().beginTransaction().add(new MenuFragment(), "menu").addToBackStack("menu").commit();

        return rootView;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mThread = new SensorThread();
        mThread.start();
    }

    public void create(Actor actor, ImageView sprite) {
        mStage.create(actor);
        sprites.add(sprite);
    }

    private void createBeer(float x, float y) {
        ImageView imBeer = new ImageView(getContext());
        imBeer.setImageResource(R.drawable.beer);
        imBeer.setId(actorId++);
        imBeer.setX(x);
        imBeer.setY(y);
        ((ViewGroup) rootView).addView(imBeer, 100, 100);
        Beer beer = new Beer(imBeer.getId(), x, y, 100, 100);
        create(beer, imBeer);
    }

    private void createObstacle(float x, float y) {
        ImageView imObstacle = new ImageView(getContext());
        imObstacle.setImageResource(R.drawable.cloud);
        imObstacle.setId(actorId++);
        imObstacle.setX(x);
        imObstacle.setY(y);
        ((ViewGroup) rootView).addView(imObstacle, 100, 100);
        Obstacle obstacle = new Obstacle(imObstacle.getId(), x, y, 100, 100);
        create(obstacle, imObstacle);
    }

    private void createTarget(float x, float y) {
        ImageView imTarget = new ImageView(getContext());
        imTarget.setImageResource(R.drawable.star);
        imTarget.setId(actorId++);
        imTarget.setX(x);
        imTarget.setY(y);
        ((ViewGroup) rootView).addView(imTarget, 100, 100);
        Target target = new Target(imTarget.getId(), x, y, 100, 100);
        create(target, imTarget);
    }

    private class SensorThread extends Thread {

        public void run() {

            waitingTime = 0;

            while(mImageChar.getHeight() == 0);

            mPerso = new Perso(R.id.image_char, mImageChar.getWidth(), mImageChar.getHeight());
            create(mPerso, mImageChar);

            mPerso.y = rootView.getHeight() - mImageChar.getHeight();

            mLife.setProgress(100);

            while(mPerso.x <= rootView.getWidth()/2) {
                mImageChar.setX(mPerso.x++);
                    mImageChar.setY(mPerso.y--);
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }



            while (mPerso.life > 0) {
                long tps = System.currentTimeMillis();

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Random rnd = new Random();
                        if (waitingTime == 0) {
                            if (rnd.nextInt(50) == 0) {
                                int obj = rnd.nextInt(3);
                                if (obj == 0) {
                                    createBeer(rnd.nextFloat()*(rootView.getWidth()-100), 0);
                                }
                                else if (obj == 1) {
                                    createObstacle(rnd.nextFloat()*(rootView.getWidth()-100), 0);
                                }
                                else if (obj == 2) {
                                    createTarget(rnd.nextFloat()*(rootView.getWidth()-100), 0);
                                }
                                waitingTime = 100;
                            }
                        }
                        else
                            waitingTime -= 1;
                    }
                });

                for(int i=0;i < mStage.actors.size(); i++)
                {
                    Actor actor = mStage.actors.get(i);
                    ImageView sprite = sprites.get(i);

                    actor.update();
                    mPerso.x += acc;

                    if(mPerso.x < 0)
                        mPerso.x = 0;

                    if(mPerso.x + mImageChar.getWidth() >= rootView.getWidth())
                        mPerso.x = rootView.getWidth() - mImageChar.getWidth();

                    if(actor.toDelete || actor.y > rootView.getHeight() && rootView.getHeight() != 0) {
                        if(actor.toDelete) {
                            MediaPlayer media = MediaPlayer.create(getContext(), R.raw.hit);
                            media.start();
                        }

                        mStage.actors.remove(i);
                        sprites.remove(i);
                        final ImageView sprite2 = sprite;

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((ViewGroup) rootView).removeView(sprite2);
                            }
                        });
                    }
                    else {
                        final Actor actor2 = actor;
                        final ImageView sprite2 = sprite;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sprite2.setX(actor2.x);
                                sprite2.setY(actor2.y);
                            }
                        });


                    }

                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mScore.setText("" + mPerso.points);
                        mAlcohol.setProgress((int)mPerso.alcohol);
                        mLife.setProgress(100*mPerso.life/Perso.LIFE_INIT);
                    }
                });

                long ticks = 16 - System.currentTimeMillis() + tps; /* In order to have 60 fps */   

               if(ticks > 0) {
                   try {
                       Thread.sleep(ticks);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }

            }

            Stage.actors.removeAll(Stage.actors);
            sprites.removeAll(sprites);
            save(mPerso.points);

            if (getFragmentManager().findFragmentByTag("menu") != null) {
                getFragmentManager().popBackStack("menu", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, new MenuFragment());
            fragmentTransaction.commit();
        }

    }

    private void save(int score){

        try {
            FileOutputStream fout = getContext().openFileOutput(MainActivity.FILE, Context.MODE_PRIVATE);
            DataOutputStream file = new DataOutputStream(fout);

            file.writeInt(score);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
/*
        DataOutputStream file;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(MainActivity.FILE)));
            writer.close();
            file = new DataOutputStream(new FileOutputStream(new File(MainActivity.FILE)));
            file.writeInt(score);
        } catch (IOException e) {
            e.printStackTrace();
        }

*/
    }


    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    }

    public void onSensorChanged(SensorEvent event) {
        if (mPerso != null) {

            float val = event.values[0];


            if (Math.abs(val) <= 1) {
                acc = 0;
            } else if (Math.abs(val) <= 4) {
                acc = 2;
            } else if (Math.abs(val) <= 7) {
                acc = 4;
            } else {
                acc = 6;
            }

            acc *= -Math.signum(val);

            int rd = 1;

            if (mPerso.alcohol >= 50 && mPerso.alcohol < 70) {
                rd = 4;
            } else if (mPerso.alcohol >= 70 && mPerso.alcohol < 90) {
                rd = 3;
            } else if (mPerso.alcohol >= 90) {
                rd = 2;
            }


            acc = ((mPerso.alcohol >= 50 && (new Random()).nextInt(rd) == 0) ? -1 : 1) * acc;
        }
    }
}
