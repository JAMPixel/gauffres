package jampixel.me.gauffres;

/**
 * Created by david on 03/03/18.
 */

public abstract class Objet extends Actor{


    public Objet(int id, float x, float y, int w, int h) {
        super(id,w,h);
        super.x = x;
        super.y = y;
    }

    public void update() {
        super.y += speed;
    }

    public abstract void effect(Perso perso);
}
