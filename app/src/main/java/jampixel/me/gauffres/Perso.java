package jampixel.me.gauffres;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;

import java.util.Random;

/**
 * Created by david on 03/03/18.
 */

public class Perso extends Actor  {
    public final static int LIFE_INIT = 5, X_INIT = 0, Y_INIT = 500;
    private Sensor _accelero;
    private SensorManager _sensors;
    public float alcohol; /* Percentage of alcohol*/
    public int points;
    public int life;

    public Perso(int id, int w, int h) {
        super(id,w,h);
        //super.speed = 0.5f;
        alcohol = 0;
        points = 0;
        life = LIFE_INIT;
        super.x = X_INIT;
        super.y = Y_INIT;
    }

    public String toString() {
        return null;
    }

    public void update() {

        for(Actor actor:Stage.actors) {
            if(this != actor && collision(actor)) {
                ((Objet) actor).effect(this);
                actor.toDelete = true; /* Will be deleted at the general update */

            }
        }

        x += (((new Random()).nextInt(2) == 1)?-1:1)*alcohol/100;

        if(alcohol > 0)
            alcohol -= 0.01;

    }

    private boolean collision(Actor actor) {
        return this.x + this.w >= actor.x && this.x <= actor.x + actor.w && this.y + this.h > actor.y && this.y <= actor.y + actor.h;
    }

}
