package jampixel.me.gauffres;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by david on 04/03/18.
 */

public class ScoresFragment extends Fragment {
    private int score;
    TextView text;
    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_scores, container, false);

        score = load();

        text = rootView.findViewById(R.id.text_highscore);
        text.setText(score + "");


        getFragmentManager().beginTransaction().add(new MenuFragment(), "menu").addToBackStack("menu").commit();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    /* read the best score */
    private int load() {
        int sc = -1;

        try {
            DataInputStream file = new DataInputStream(getContext().openFileInput(MainActivity.FILE));
            sc = file.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sc;
    }
}
